﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kriegspiel.Lib.Messages
{
    public interface IMessage
    {
        string ToJsonString();
        void FromJsonString(string jsonString);
    }
}