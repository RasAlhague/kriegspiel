﻿using Newtonsoft.Json;
using System;

namespace Kriegspiel.Lib.Messages
{
    public class Ping : IMessage
    {
        public string Type { get; private set; }
        public DateTime SendTime { get; set; }
        public DateTime ReceiveTime { get; set; }

        public Ping()
        {
            SendTime = DateTime.Now;
            ReceiveTime = DateTime.MinValue;
            Type = "PING";
        }

        public double CalculatePing()
        {
            if (ReceiveTime != DateTime.MinValue)
            {
                return ReceiveTime.Subtract(SendTime).TotalMilliseconds;
            }
            else
            {
                return 0;
            }
        }

        public string ToJsonString()
        {
            Ping ping = new Ping() { SendTime = SendTime, ReceiveTime = ReceiveTime };

            return JsonConvert.SerializeObject(ping);
        }

        public void FromJsonString(string jsonString)
        {
            Ping ping = JsonConvert.DeserializeObject<Ping>(jsonString);

            this.SendTime = ping.SendTime;
            this.ReceiveTime = ping.ReceiveTime;
        }
    }
}
