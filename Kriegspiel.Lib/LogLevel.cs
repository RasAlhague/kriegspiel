﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kriegspiel.Lib
{
    public enum LogLevel
    {
        Debug,
        Info,
        Warning,
        Error
    }
}
