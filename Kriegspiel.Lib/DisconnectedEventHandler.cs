﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Kriegspiel.Lib
{
    public delegate void DisconnectedEventHandler(IPEndPoint iPEndPoint, DisconnectReason reason);

    public enum DisconnectReason
    {
        Unknown,
        ConnectionLost,
        ServerShutdown,
        ClientShutdown,
        ServerCutConnection,
        ClientCutConnection
    }
}
