﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace Kriegspiel.Lib.Server
{
    public class KriegspielServer
    {
        private readonly TcpListener _server;
        private readonly ConcurrentDictionary<Guid, KriegspielReceiver> _receivers;
        private readonly ReceiverCallbacks _callbacks;

        public IPEndPoint LocalEndpoint => _server.LocalEndpoint as IPEndPoint;
        public ServerState State { get; private set; }
        public IEnumerable<KriegspielReceiver> Receivers => _receivers.Values;

        public KriegspielServer(IPAddress iPAddress, int port, ReceiverCallbacks callbacks)
        {
            _server = new TcpListener(iPAddress, port);
            _receivers = new ConcurrentDictionary<Guid, KriegspielReceiver>();
            _callbacks = callbacks;
            State = ServerState.Stopped;
        }

        public async Task StartAsync()
        {
            _server.Start();
            State = ServerState.Listening;

            while (State == ServerState.Listening)
            {
                var client = await _server.AcceptTcpClientAsync();
                var receiver = new KriegspielReceiver(client, this, _callbacks);

                _receivers.TryAdd(receiver.Id, receiver);

                receiver.CheckConnectionLost();
                receiver.Run();
            }
        }

        public void RemoveReceiver(Guid id)
        {
            _receivers.TryRemove(id, out KriegspielReceiver kriegspielReceiver);
        }
    }
}