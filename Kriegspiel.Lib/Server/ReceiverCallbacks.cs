﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kriegspiel.Lib.Server
{
    public class ReceiverCallbacks
    {
        public ConnectedEventHandler ConnectedEventHandler { get; set; }
        public DisconnectedEventHandler DisconnectedEventHandler { get; set; }
        public SendOutputEventHandler SendOutputEventHandler { get; set; }
    }
}