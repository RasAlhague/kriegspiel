﻿using Kriegspiel.Lib.Messages;
using System;
using System.Collections.Concurrent;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;

namespace Kriegspiel.Lib.Server
{
    public class KriegspielReceiver
    {
        public event ConnectedEventHandler Connected;
        public event DisconnectedEventHandler Disconnected;
        public event SendOutputEventHandler SendOutput;

        private readonly IPEndPoint _remoteEndpoint;
        private readonly KriegspielServer _server;
        private readonly TcpClient _client;

        private readonly Task _reader;
        private readonly Task _writer;

        private CancellationTokenSource _tokenSource;
        private CancellationToken _cancellationToken;

        private readonly StreamReader _sr;
        private readonly StreamWriter _sw;

        private readonly ConcurrentQueue<string> _messageQueue;

        private DateTime _lastPingResponse;

        public ReceiverState State { get; private set; }

        public Guid Id { get; }

        public KriegspielReceiver(TcpClient remoteClient, KriegspielServer server, ReceiverCallbacks callbacks)
        {
            _server = server;
            _client = remoteClient;

            State = ReceiverState.Pending;

            var ipEnd = _client.Client.RemoteEndPoint as IPEndPoint;
            _remoteEndpoint = new IPEndPoint(ipEnd.Address, ipEnd.Port);

            _tokenSource = new CancellationTokenSource();
            _cancellationToken = _tokenSource.Token;

            _reader = CreateReader();
            _writer = CreateWriter();

            _sr = new StreamReader(_client.GetStream());
            _sw = new StreamWriter(_client.GetStream());

            _lastPingResponse = DateTime.Now;

            _messageQueue = new ConcurrentQueue<string>();

            Id = Guid.NewGuid();

            SetCallbacks(callbacks);

            OnConnected();
        }

        public void Run()
        {
            State = ReceiverState.Running;

            _reader.Start();
            _writer.Start();
        }
        public void CheckConnectionLost()
        {
            Task.Run(async () =>
            {
                while (State != ReceiverState.Disconnected)
                {
                    //Ping ping = new Ping();

                    //_messageQueue.Enqueue(ping.ToJsonString());

                    await Task.Delay(1000);

                    if (DateTime.Now.Subtract(_lastPingResponse).TotalMilliseconds > 10000)
                    {
                        Disconnect(DisconnectReason.ConnectionLost);
                        break;
                    }
                    if (_cancellationToken.IsCancellationRequested)
                    {
                        break;
                    }
                }
            }, _cancellationToken);
        }

        public void Disconnect()
        {
            Disconnect(DisconnectReason.Unknown);
        }

        public void Disconnect(DisconnectReason reason)
        {
            State = ReceiverState.Disconnected;

            _tokenSource.Cancel();
            _client.Close();
            _server.RemoveReceiver(Id);

            OnDisconnected(reason);
        }

        private Task CreateReader()
        {
            return new Task(async () =>
            {
                while (State == ReceiverState.Running || !_tokenSource.IsCancellationRequested)
                {
                    try
                    {
                        if (_tokenSource.IsCancellationRequested)
                        {
                            return;
                        }
                        string message = await _sr.ReadLineAsync();

                        ProcessMessage(message);
                    }
                    catch (Exception ex)
                    {
                        OnSendOutput(ex.ToString(), LogLevel.Error);
                        Disconnect(DisconnectReason.ClientCutConnection);

                        return;
                    }
                }
            }, _cancellationToken);
        }

        private Task CreateWriter()
        {
            return new Task(async () =>
            {
                while (State == ReceiverState.Running || !_tokenSource.IsCancellationRequested)
                {
                    if (_messageQueue.TryDequeue(out string message))
                    {
                        try
                        {
                            await _sw.WriteLineAsync(message);
                            await _sw.FlushAsync();

                            OnSendOutput($"will be send <{message}>", LogLevel.Debug);
                        }
                        catch (Exception ex)
                        {
                            OnSendOutput(ex.ToString(), LogLevel.Error);
                            Disconnect(DisconnectReason.ClientCutConnection);

                            return;
                        }
                    }
                    else
                    {
                        await Task.Delay(1);
                    }
                }
            }, _cancellationToken);
        }

        private void ProcessMessage(string message)
        {
            if (message.Contains("PING"))
            {
                Ping ping = new Ping();

                ping.FromJsonString(message);

                if (ping.ReceiveTime == DateTime.MinValue)
                {
                    ProcessPingRequest(ping);
                }
                else
                {
                    ProcessPingResponse(ping);
                }
            }
        }

        private void ProcessPingRequest(Ping request)
        {
            request.ReceiveTime = DateTime.Now;

            _lastPingResponse = request.ReceiveTime;

            _messageQueue.Enqueue(request.ToJsonString());

            OnSendOutput("Received ping request", LogLevel.Debug);
        }

        private void ProcessPingResponse(Ping response)
        {
            _lastPingResponse = response.ReceiveTime;
            OnSendOutput("Received ping response", LogLevel.Debug);
        }

        private void SetCallbacks(ReceiverCallbacks callbacks)
        {
            Connected += callbacks.ConnectedEventHandler;
            Disconnected += callbacks.DisconnectedEventHandler;
            SendOutput += callbacks.SendOutputEventHandler;
        }

        protected void OnConnected()
        {
            Connected?.Invoke(_remoteEndpoint);
        }

        protected void OnDisconnected(DisconnectReason reason)
        {
            Disconnected?.Invoke(_remoteEndpoint, reason);
        }

        protected void OnSendOutput(string message, LogLevel logLevel)
        {
            SendOutput?.Invoke(_remoteEndpoint, message, logLevel);
        }
    }
}