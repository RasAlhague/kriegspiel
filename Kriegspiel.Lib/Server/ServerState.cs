﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kriegspiel.Lib.Server
{
    public enum ServerState
    {
        Stopped,
        Listening
    }
}
