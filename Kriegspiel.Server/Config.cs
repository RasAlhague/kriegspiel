﻿using Kriegspiel.Lib;
using Options;
using Options.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Kriegspiel.Server
{
    public class Config
    {
        protected static Config _config;

        public LogLevel LogLevel { get; private set; }

        protected Config()
        {
        }

        public static Config SetConfig(IEnumerable<string> args)
        {
            if (_config == null)
            {
                _config = new Config();
            }
            if(args.Any(x => x.Contains("--logLevel=")))
            {
                _config.LogLevel = ParseLogLevel(args.First(x => x.Contains("--logLevel=")));
            }

            return _config;
        }

        public static Config GetInstance()
        {
            return _config;
        }

        private static LogLevel ParseLogLevel(string logArg)
        {
            Enum.TryParse(logArg.Replace("--logLevel=", ""), out LogLevel logLevel);

            return logLevel;
        }
    }
}
