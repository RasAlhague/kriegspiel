﻿using Kriegspiel.Lib;
using Kriegspiel.Lib.Server;
using System;
using System.Net;

namespace Kriegspiel.Server
{
    class Program
    {
        static void Main(string[] args)
        {
            Config config = Config.SetConfig(args);

            ReceiverCallbacks receiverCallbacks = new ReceiverCallbacks();
            receiverCallbacks.ConnectedEventHandler = ConnectedCallback;
            receiverCallbacks.DisconnectedEventHandler = DisconnectedCallback;
            receiverCallbacks.SendOutputEventHandler = SendOutputCallback;

            var server = new KriegspielServer(IPAddress.Any, 1234, receiverCallbacks);
            server.StartAsync();

            Console.ReadKey();
        }

        public static void ConnectedCallback(IPEndPoint iPEndPoint)
        {
            Console.WriteLine($"Client connected on: <{iPEndPoint.Address.ToString()}::{iPEndPoint.Port}>");
        }

        public static void DisconnectedCallback(IPEndPoint iPEndPoint, DisconnectReason reason)
        {
            Console.WriteLine($"Client disconnected on: <{iPEndPoint.Address.ToString()}::{iPEndPoint.Port}>");
        }

        public static void SendOutputCallback(IPEndPoint iPEndPoint, string message, LogLevel logLevel)
        {
            Config conf = Config.GetInstance();

            if (conf.LogLevel == LogLevel.Debug)
            {
                Console.WriteLine($"DEBUG: <{iPEndPoint.Address.ToString()}::{iPEndPoint.Port}> send: {message}");
            }
            if (conf.LogLevel == LogLevel.Info)
            {
                Console.WriteLine($"INFO: <{iPEndPoint.Address.ToString()}::{iPEndPoint.Port}> send: {message}");
            }
            if (conf.LogLevel == LogLevel.Warning)
            {
                Console.WriteLine($"WARNING: <{iPEndPoint.Address.ToString()}::{iPEndPoint.Port}> received: {message}");
            }
            if (conf.LogLevel == LogLevel.Error)
            {
                Console.WriteLine($"ERROR: <{iPEndPoint.Address.ToString()}::{iPEndPoint.Port}> received: {message}");
            }
            
        }
    }
}