﻿using Kriegspiel.Lib.Messages;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Kriegspiel.Server.TestClient
{
    static class Program
    {
        static void Main(string[] args)
        {
            //TestConnect();
            //TestWritingToServer();
            TestPingingToServer();
        }

        static void TestConnect()
        {
            Console.WriteLine("Testing Connecting...");

            using (var client = new TcpClient())
            {
                client.Connect(IPAddress.Parse("127.0.0.1"), 1234);

                Console.WriteLine($"--> Successfully connected to {client.Client.RemoteEndPoint.ToString()}");
            }
        }

        static void TestWritingToServer()
        {
            Console.WriteLine("Testing writing to server");
            using (var client = new TcpClient())
            {
                client.Connect(IPAddress.Parse("127.0.0.1"), 1234);

                Console.WriteLine($"--> Successfully connected to {client.Client.RemoteEndPoint.ToString()}");

                StreamWriter sw = new StreamWriter(client.GetStream());

                sw.WriteLine("HI");
                sw.Flush();

                Console.WriteLine($"--> Successfully written to {client.Client.RemoteEndPoint.ToString()}");
            }
        }

        static void TestPingingToServer()
        {
            Console.WriteLine("Testing pinging to server");

            using (var client = new TcpClient())
            {
                client.Connect(IPAddress.Parse("127.0.0.1"), 1234);

                StreamWriter sw = new StreamWriter(client.GetStream());
                StreamReader sr = new StreamReader(client.GetStream());

                while (true)
                {
                    Console.WriteLine("");
                    Console.WriteLine("");

                    string message = JsonConvert.SerializeObject(new Ping());

                    Console.WriteLine(message);

                    sw.WriteLine(message);
                    sw.Flush();

                    Console.WriteLine("Send Data...");

                    var resp = sr.ReadLine();

                    Ping ping = JsonConvert.DeserializeObject<Ping>(resp);

                    Console.WriteLine("current: " + resp);

                    Console.WriteLine($"--> Successfully pinged to {client.Client.RemoteEndPoint.ToString()}, in {ping.CalculatePing()} miliseconds.");

                    Thread.Sleep(1000);
                }
            }
        }
    }
}